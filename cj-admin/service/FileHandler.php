<?php

/**
 * Class FileHandler
 *
 * This class offers some basic functionality for
 * handling file access
 */
class FileHandler
{
    public static $errorMessage = '';

    /**
     * This method returns a multidimensional array containing an array of all HTML files and JSON files.
     * array(
     *   "html" => @see FileHandler::getHtmlFiles(),
     *   "json" => @see FileHandler::getJsonFiles()
     * )
     *
     * @return array an associative array with all site and content files
     */
    public function getHtmlAndJsonFiles(): array
    {
        return array(
          "html" => $this->getHtmlFiles(),
          "json" => $this->getJsonFiles()
        );
    }

    /**
     * This method returns an array of all existing html,
     * html, php and php3 files in the root directory.
     *
     * @return array an array containing all site files in the root directory
     */
    public function getHtmlFiles()
    {
        $htmlFiles = glob('../../*.{html,htm,php,php3}', GLOB_BRACE);
        $result = array();
        foreach ($htmlFiles as $htmlFile) {
            $file = substr($htmlFile, 6);
            $result[] = $file;
        }
        return $result;
    }

    /**
     * This method returns an array of all content json files in that specific directory.
     *
     * @return array an array containing all content json files
     */
    public function getJsonFiles()
    {
        $jsonFiles = glob('../../cj-content/sites/*.{json}', GLOB_BRACE);

        $result = array();
        foreach ($jsonFiles as $jsonFile) {
            $jsonFilename = substr($jsonFile, 6);
            $result[] = $jsonFilename;
        }
        return $result;
    }

    /**
     * Returns array of json files
     *
     * @param string $path
     * @return array
     */
    public function getContentOrPostFiles($path)
    {
        $jsonFiles = glob('../..' . $path . '*.{json}', GLOB_BRACE);

        $result = array();
        foreach ($jsonFiles as $jsonFile) {
            $result[] = $jsonFile;
        }
        return $result;
    }

    /**
     * Returns latest (last modified) file
     *
     * @param string $path
     * @return string
     */
    public function getNewestFile($path)
    {
        $jsonFiles = glob('../..' . $path . '*.{json}', GLOB_BRACE);

        $latestTime = 0;
        $lastModifiedFile = '';
        foreach ($jsonFiles as $jsonFile) {
            if (filemtime($jsonFile) > $latestTime){
                $lastModifiedFile = $jsonFile;
            }
        }
        return $lastModifiedFile;
    }

    /**
     * Returns latest (last modified) files
     *
     * @param string $path
     * @return array
     */
    public function getNewestFiles($path)
    {
        $jsonFiles = glob('../..' . $path . '*.{json}', GLOB_BRACE);

        $fileList = [];
        foreach ($jsonFiles as $jsonFile) {
            $fileList[filemtime($jsonFile)] = substr($jsonFile, 23);
        }
        ksort($fileList);
        return array_reverse($fileList);
    }

    /**
     * Returns array with filename and language of
     * given post file handle
     *
     * @param $file
     * @return array
     */
    public function getFileAndLang($file)
    {
        $s = explode(".", $file);
        $beautifiedFilename = explode("_", $s[0]);
        return $result[] = [
            'file' => $beautifiedFilename[0],
            'lang' => $beautifiedFilename[1]
        ];
    }


    /**
     * This method returns an associative array of all
     * available users from the .shadow file
     *
     * @return array|bool an array of all registered users or false if the file does not exist
     */
    public static function getUsers()
    {
        if (file_exists("../etc/.shadow")) {
            $fn = fopen("../etc/.shadow", "r") or die("failed to open file");

            $credentials = array();
            while ($row = fgets($fn)) {
                list($username, $pw, $type) = explode(":", $row);

                $credentials[] = array(
                  'username' => $username,
                  'password' => $pw,
                  'type' => $type,
                );
            }
            fclose($fn);
            return $credentials;
        } else {
            return false;
        }
    }

    /**
     * Checks html for post cj classes, e.g. cj-post, cj-date, cj-headline
     *
     * @param $file
     * @return int
     */
    public static function getPostsCount($file)
    {
        $doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $doc->loadHTMLFile("../../" . $file);
        // No error output due to html 5 elements
        libxml_clear_errors();

        try {
            $elements = $doc->getElementsByTagName('span');
        } catch (Exception $e) {
            // TODO handle error
        }
        if (!is_null($elements)) {
            $headlineCount = 0;
            $postCount = 0;
            $dateCount = 0;
            foreach ($elements as $element) {
                $class = $element->getAttribute('class');
                if (strpos($class, 'cj-headline') !== false) {
                    $headlineCount++;
                }
                if (strpos($class, 'cj-post') !== false) {
                    $postCount++;
                }
                if (strpos($class, 'cj-date') !== false) {
                    $dateCount++;
                }
            }
            $resultCount = max(max($headlineCount, $postCount), max($postCount, $dateCount));
            if ($resultCount == 0) {
                self::$errorMessage = 'No cj-headline, cj-post or cj-date classes found in ' . $file . '.';
                return 0;
            } else {
                return $resultCount;
            }
        } else {
            self::$errorMessage = 'No cj-headline, cj-post or cj-date classes found in ' . $file . '.';
            return 0;
        }
    }

    /**
     * Uploads and replaces logo file of the login screen
     */
    public function uploadFile()
    {
        $target_dir = "../resources/img/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
        // Check if image file is a actual image or fake image
        if(isset($_POST["uploadFile"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                $uploadOk = 1;
            } else {
                echo '<span class="infoMessage">File is not an image.</span>';
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo '<span class="infoMessage">Sorry, file already exists.</span>';
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 2300000) {
            echo '<span class="infoMessage">Image files must no be larger than 2MB.</span>';
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "png") {
            echo '<span class="infoMessage">Sorry, only PNG files are allowed.</span>';
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo '<span class="infoMessage">Your file was not uploaded.</span>';
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo '<span class="infoMessage">The file <i>'. basename( $_FILES["fileToUpload"]["name"]). '</i> has been uploaded.</span>';

                if (!file_exists($target_dir . 'oldLogo.png')){
                    rename($target_dir . 'logo.png', $target_dir . 'oldLogo.png');
                    rename($target_file, $target_dir . 'logo.png');
                }elseif (file_exists($target_dir . 'oldLogo.png')){
                    rename($target_file, $target_dir . 'logo.png');
                }
            } else {
                echo '<span class="infoMessage">Sorry, an error occurred.</span>';
            }
        }
    }

    /**
     * Resets the logo of the login screen
     */
    public function resetLogo()
    {
        $target_dir = "../resources/img/";
        $oldLogo = 'oldLogo.png';

        if (file_exists($target_dir . $oldLogo)){
            rename($target_dir . $oldLogo, $target_dir . 'logo.png');
        }
    }

    /**
     * Shortens a given string
     *
     * @param $string
     * @param $length
     * @param string $dots
     * @return string
     */
    public function truncate($string, $length, $dots = "...")
    {
        return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
    }
}
