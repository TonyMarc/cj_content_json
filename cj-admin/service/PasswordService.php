<?php

/**
 * Class PasswordService
 *
 * Hashes strings and compares strings and hashes
 */
class PasswordService
{
    /**
     * Compares password strings
     *
     * @param $password
     * @param $passwordRepeat
     * @return bool
     */
    public function comparePasswords($password, $passwordRepeat)
    {
        if (strcmp($password, $passwordRepeat) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Creates a hash of a given password
     *
     * @param $password
     * @return bool|string
     */
    public static function createHash($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * Checks if the given password matches the hash
     *
     * @param $password
     * @param $hash
     * @return bool
     */
    public static function verifyPassword($password, $hash)
    {
        return password_verify($password, $hash);
    }
}
