/**
 * Compares password with input pattern
 *
 * @param argument
 */
function comparePasswords(argument) {
    var input = document.getElementById('inputNewPw');
    var pattern = input.value;

    argument.pattern = escapeRegExp(pattern);
}

/**
 * Compares password with input pattern
 *
 * @param argument
 */
function compareNewUserPasswords(argument) {
    var input = document.getElementById('inputPw');
    var pattern = input.value;

    argument.pattern = escapeRegExp(pattern);
}

/**
 * Escapes regex characters
 *
 * @param string
 */
function escapeRegExp(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}
