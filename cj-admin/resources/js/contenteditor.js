function insertFormFields(argument) {
    var node = argument;
    console.log(node);
    console.log(node.parentNode);
    node.parentNode.insertAdjacentHTML('beforebegin',
        '<div class="card"><img src="../resources/img/delete.svg" id="deleteButton" title="Delete content element" class="remove" onclick="showDeleteModal(this)"><img src="../resources/img/addButton.svg" id="addButton" class="add" title="Add content element" onclick="showAddModal(this)"><div class="cardMain"><label><h3>Custom</h3></label><input type="hidden" name="modified_content_parents[]" value="Custom"/></div><div class="cardFooter"><textarea rows="5" placeholder="Insert text" name="modified_content[]"></textarea></div></div>');
}

function removeFormFields(argument) {
    var node = argument;
    node.parentNode.parentNode.removeChild(node.parentNode);
}

function showAddModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalAddDialog');
    modal.style.display = "block";

// Get the <span> element that closes the modal
    var span = document.getElementById('closeAddDialog');

    var buttonNo = document.getElementById("addNo");
    var buttonYes = document.getElementById("addYes");


    // Closes dialog box
    span.onclick = function() {
        modal.style.display = "none";
    };

    // Closes dialog box
    buttonNo.onclick = function () {
        modal.style.display = "none";
    };

    // Removes form fields
    buttonYes.onclick = function () {
        insertFormFields(argument);
        modal.style.display = "none";
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}

function showDeleteModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalDeleteDialog');
    modal.style.display = "block";

// Get the <span> element that closes the modal
    var span = document.getElementById('closeDeleteDialog');

    var buttonNo = document.getElementById("deleteNo");
    var buttonYes = document.getElementById("deleteYes");


    // Closes dialog box
    span.onclick = function() {
        modal.style.display = "none";
    };

    // Closes dialog box
    buttonNo.onclick = function () {
        modal.style.display = "none";
    };

    // Removes form fields
    buttonYes.onclick = function () {
        removeFormFields(argument);
        modal.style.display = "none";
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}

