/**
 * Displays change password form
 *
 * @param argument
 */
function showChangePasswordModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalChangePasswordDialog');
    modal.style.display = "block";

    // Get the <span> element that closes the modal
    var span = document.getElementById('closeChangePasswordDialog');
    var headline = document.getElementById('headlineChangePasswordDialog');
    var buttonChangePassword = document.getElementById('changePassword');

    buttonChangePassword.value = argument.value;

    var headlineBackup = headline.innerHTML;
    headline.innerHTML += argument.value + '?';

    // Closes dialog box
    span.onclick = function () {
        modal.style.display = "none";
        headline.innerHTML = headlineBackup;
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
            headline.innerHTML = headlineBackup;
        }
    };
}

/**
 * Displays user deletion form
 *
 * @param argument
 */
function showDeleteModal(argument) {

    // Get the modal
    var modal = document.getElementById('modalDeleteDialog');
    modal.style.display = "block";

// Get the <span> element that closes the modal
    var span = document.getElementById('closeDeleteDialog');

    var buttonNo = document.getElementById("deleteNo");
    var buttonYes = document.getElementById("deletion-login");

    buttonYes.value = argument.value;

    // Closes dialog box
    span.onclick = function () {
        modal.style.display = "none";
    };

    // Closes dialog box
    buttonNo.onclick = function () {
        modal.style.display = "none";
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}
