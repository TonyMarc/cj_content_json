/**
 * Displays content-sensitive help modal
 */
function showHelpModal() {

    // Get the modal
    var modal = document.getElementById('helpModal');
    modal.style.display = "block";

    // Get the <span> element that closes the modal
    var span = document.getElementById('closeHelpModal');

    // Get user menu and overlay
    var userMenu = document.getElementById("userMenu");
    var overlay = document.getElementById("overlay");

    // close user menu and turn off overlay
    overlay.style.display = "none";
    userMenu.className = "userMenu";

    // Closes help dialog
    span.onclick = function() {
        modal.style.display = "none";
    };

    // Closes modal if click is registered outside the dialog box
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    };
}
